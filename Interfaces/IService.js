/**
 * Created by Travin on 15.01.2015.
 */
function IService(){
    this.getResult = function(parameters){throw new Error("You didn't implement IService or didn't implement getResult method in your service implimentation");};

    this.parseResult = function(resultObj){throw new Error("You didn't implement IService or didn't implement parseResult method in your service implimentation");};
}