/**
 * Created by Travin on 30.12.2014.
 */

/*
resultData signature:
    [
        {
            blocks: [{
                filterId: '' ,
                text: ''
            }],
            fullText: ''
        },
        ...
    ]
*/
function CustomSelectList(parameters) {

    var self = this;

    this._parameters = {
        $inputElement: '',
        fullAdressBlockContainerClass: 'adress-blocks-container',
        fullAdressBlockClass: 'main-adress-block',
        atomarAdressBlockClass: 'adress-block',
        atomarAdressBlockSpanClass: 'adress-block-span',
        deleteLinkSource: 'CustomSelect\\delete.png',
        changeStateFromAutocomplete: false,
        service: new IService()
    };

    this._init = function () {
        $().extend(this._parameters, parameters);
        this._parameters.$inputElement = $(this._parameters.$inputElement);
        createAndSetStylesAdressBlockContainer();
        createBindings();
        createJQueryAutocomplete();
    };

    function createAndSetStylesAdressBlockContainer(){
        self._parameters.$fullAdressBlockContainer = $('<div class="' + self._parameters.fullAdressBlockContainerClass + '"></div>').appendTo(self._parameters.$inputElement.parent());
        self._parameters.$fullAdressBlockContainer.offset({
            top: self._parameters.$inputElement.offset().top,
            left: self._parameters.$inputElement.offset().left
        });
        self._parameters.$fullAdressBlockContainer.height(self._parameters.$inputElement.height() + 2);
        self._parameters.$fullAdressBlockContainer.css('border', self._parameters.$inputElement.css('border'));
        self._parameters.$fullAdressBlockContainer.css('background', self._parameters.$inputElement.css('background'));
        self._parameters.$inputElement.css('padding-left', '10px');
        self._parameters.$fullAdressBlockContainer.css('border-right-style', 'hidden');
        self._parameters.$fullAdressBlockContainer.css('border-radius', self._parameters.$inputElement.css('border-radius'));
    };

    function putList(parent, item, text) {
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append(text)
            .appendTo(parent);
    };

    function addDeleteLinkAndActionForSelectedItem(parent) {
        var deleteLink = $('<img class="deleteLink" src="' + self._parameters.deleteLinkSource + '">').appendTo(parent);
        deleteLink.click(function (e) {
            deleteAdress(e, $(this).parent());
        });
    };

    function setItem(item) {
        if (self._parameters.$inputElement.children('.' + self._parameters.fullAdressBlockClass + '[selected-blocks != "selected"]').length == 0 && item) {
            showFocusedItem(item);
        }
        var inputWidth = self._parameters.$fullAdressBlockContainer.width(),
            adressBlock = self._parameters.$fullAdressBlockContainer.children('.' + self._parameters.fullAdressBlockClass + '[selected-blocks != "selected"]').attr('selected-blocks', 'selected');
        addDeleteLinkAndActionForSelectedItem(adressBlock.children('.' + self._parameters.atomarAdressBlockClass));
        inputWidth -= self._parameters.$fullAdressBlockContainer.width();
        self._parameters.$inputElement.offset({left: (self._parameters.$fullAdressBlockContainer.position().left + self._parameters.$fullAdressBlockContainer.width())}).width(self._parameters.$inputElement.width() + inputWidth);
    };

    function showFocusedItem(item) {
        var totalWidth = self._parameters.$inputElement.width() + self._parameters.$fullAdressBlockContainer.width();
        self._parameters.$fullAdressBlockContainer.offset({
            top: self._parameters.$inputElement.position().top,
            left: (self._parameters.$inputElement.position().left - self._parameters.$fullAdressBlockContainer.width())
        });

        self._parameters.$fullAdressBlockContainer.children('.' + self._parameters.fullAdressBlockClass).remove();
        var mainAdressBlock = $('<div class="' + self._parameters.fullAdressBlockClass + '"></div>').appendTo(self._parameters.$fullAdressBlockContainer);
        var bloksString = '';
        for (var i = 0; i < item.blocks.length; i++) {
            bloksString += '<div class="' + self._parameters.atomarAdressBlockClass + '" data-uuid="' + item.blocks[i].filterId + '"><span>' + item.blocks[i].text + '</span></div>';
        }
        $(mainAdressBlock).append(bloksString);
        self._parameters.$fullAdressBlockContainer.removeAttr('hidden');
        self._parameters.$inputElement.offset({left: (self._parameters.$fullAdressBlockContainer.position().left + self._parameters.$fullAdressBlockContainer.width())}).width(totalWidth - self._parameters.$fullAdressBlockContainer.width());
    };

    function deleteAdress(e, adressBlock) {
        var totalWidth = self._parameters.$inputElement.width() + self._parameters.$fullAdressBlockContainer.width();
        $(adressBlock).attr('deleted', 'deleted');
        var blocksList = $(adressBlock).parent().children();
        for (var i = 0; i < blocksList.length - 1; i++) {
            if ($(blocksList[i]).attr('deleted')) {
                $(blocksList[i + 1]).attr('deleted', 'deleted');
            }
        }
        $('[deleted="deleted"]').remove();
        self._parameters.$inputElement.offset({left: (self._parameters.$fullAdressBlockContainer.position().left + self._parameters.$fullAdressBlockContainer.width())}).width(totalWidth - self._parameters.$fullAdressBlockContainer.width());
    };

    function getFilterValue() {
        var blocks = $('.' + self._parameters.fullAdressBlockClass).children();
        if (blocks && blocks.length > 0) {
            return $(blocks[blocks.length - 1]).attr('data-uuid');
        }
        return null;
    };

    function createBindings() {
        self._parameters.$inputElement.bind('input blur', function () {
            if (self._parameters.$fullAdressBlockContainer && self._parameters.$fullAdressBlockContainer.children('.' + self._parameters.fullAdressBlockClass + '[selected-blocks != "selected"]').length > 0) {
                setItem();
            }
        });

        self._parameters.$inputElement.bind('keydown', function (e) {
            if (e.keyCode != 8 || self._parameters.$inputElement.val()) {
                return;
            }
            if (self._parameters.$fullAdressBlockContainer.children().length > 0) {
                var lastMainAdressBlock = self._parameters.$fullAdressBlockContainer.children()[self._parameters.$fullAdressBlockContainer.children().length - 1];
                var lastAdressBlock = $(lastMainAdressBlock).children()[$(lastMainAdressBlock).children().length - 1];
                deleteAdress(e, lastAdressBlock);
            }
        });
    };

    function createJQueryAutocomplete(){
        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
            var span = '';
            for (var i = 0; i < item.blocks.length; i++) {
                span += '<span class="' + self._parameters.atomarAdressBlockSpanClass + ((i == item.blocks.length - 1) ? ' last' : '') + '">' + item.blocks[i].text + '</span>'
            }
            return putList(ul, item, span);
        };

        self._parameters.$inputElement.autocomplete({
            source: function(request, response){
                if(request.term.length > 0){
                    var params = {
                        keyWord: request.term,
                        filterValue: getFilterValue()
                    };
                    var result = self._parameters.service.getResult(params);
                    response(result);
                }
                else{
                    response([]);
                }
            },
            select: function(event, ui){
                setItem(ui.item);
                event.preventDefault();
                $('.ui-autocomplete').children().remove();
            },
            focus: function( event, ui ) {
                $('.myText').text('');
                showFocusedItem(ui.item);
                this.value = '';
                event.preventDefault();
            }
        });
    };

    this._init();
};